#!/usr/bin/env python3
import random
import time
import tkinter as tk
"""
A Gui made with Tkinter. 
Displays a number between zero and nine
that can be changed to a random number with mouse 
click or to a specific number with a key press.
"""
class NumDisplay:
    def __init__(self,master):
        self.master = master
        self.title = self.master.title('Number Display')
        self.geo = self.master.geometry('400x400')
        self.master.resizable(width=False, height=False)
        
        self.num_var = tk.IntVar()
        self.num_var.set(0)
        self.num_label =tk.Label(self.master, textvariable=self.num_var,
                                 foreground='white', font=(None,150))                    
                                 
        self.num_label.place(relx = 0.5, rely = 0.5,anchor = 'c')
        self.instruction_label = tk.Label(self.master, text='Press Keys 0-9 to change value\n'
                                          'Click to generate a random Value')
                                          
        self.instruction_label.place(x= 0, y= 350)
        self.master.bind('<Escape>',lambda e: self.master.destroy())
        self.master.bind(1,self.on_press)
        self.master.bind('<Key>',self.on_press)
        self.master.bind('<Button-1>',lambda e: self.num_var.set(random.randint(0,9)))
    
    def on_press(self,event):
        """
        Changes the label to 
        one of the corresponding 
        number keys.
        
        Parameters
        ----------
        Event(String):
            Special Event String
        
        Output(None)
        """
        if event.keysym in ('0','1','2','3','5','4','6','7','8','9'):
            self.num_var.set(event.keysym)     
        
if __name__ == "__main__":    
    root = tk.Tk()
    num_app = NumDisplay(root)
    tk.mainloop()
